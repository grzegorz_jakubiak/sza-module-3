package com.dbauth.auth;

import com.dbauth.authority.Authoritiy;
import com.dbauth.authority.AuthorityRepository;
import com.dbauth.token.VerificationToken;
import com.dbauth.token.VerificationTokenRepository;
import com.dbauth.user.UserService;
import com.dbauth.util.LoginUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.UUID;

@RestController
public class AuthRestControler {

    private final LoginUtil loginUtil;
    private final UserService userService;
    private final VerificationTokenRepository verificationTokenRepository;

    public AuthRestControler(LoginUtil loginUtil, UserService userService, VerificationTokenRepository verificationTokenRepository) {
        this.loginUtil = loginUtil;
        this.userService = userService;
        this.verificationTokenRepository = verificationTokenRepository;
    }

    @GetMapping("/forAll")
    public String forAll(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        if (principal != null) {
            return "Cześć " + principal.getName()+", zalogowałeś się " + loginUtil.getNumberAttemptAuth(request, response, principal) + " razy.";
        } else {
            return "Cześć nieznajomy";
        }

    }

    @GetMapping("/forUser")
    public String forUser(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        return "Cześć user: " + principal.getName() + ", zalogowałeś się " + loginUtil.getNumberAttemptAuth(request, response, principal) + " razy.";
    }

    @GetMapping("/forAdmin")
    public String forAdmin(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        return "Cześć admin: " + principal.getName() + ", zalogowałeś się " + loginUtil.getNumberAttemptAuth(request, response, principal) + " razy.";
    }

    @GetMapping("/success-logout")
    public String logout() {
        return "Papa";
    }

    @GetMapping("/verify-admin")
    public void verifyAdmin(@RequestParam String adminToken) {
        userService.verifyAdmin(adminToken);
        verificationTokenRepository.deleteById(UUID.fromString(adminToken));
    }
}
