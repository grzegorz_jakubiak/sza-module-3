package com.dbauth.authority;

public enum Role {

    ROLE_ADMIN, ROLE_USER;

    public String getName(){
        return this.name();
    }
}
