package com.dbauth.token;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface VerificationTokenRepository extends CrudRepository<VerificationToken, UUID> {

    VerificationToken findByTokenValue(UUID value);
}
