package com.dbauth.user;

import com.dbauth.authority.Authoritiy;
import com.dbauth.authority.AuthorityRepository;
import com.dbauth.authority.Role;
import com.dbauth.mail.MailService;
import com.dbauth.token.VerificationToken;
import com.dbauth.token.VerificationTokenRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;


@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final VerificationTokenRepository verificationTokenRepository;
    private final MailService mailService;
    private final AuthorityRepository authoritiyRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, VerificationTokenRepository verificationTokenRepository, MailService mailService, AuthorityRepository authoritiyRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.verificationTokenRepository = verificationTokenRepository;
        this.mailService = mailService;
        this.authoritiyRepository = authoritiyRepository;
    }

    public void addNewUser(User user, HttpServletRequest request) {
        if(user.getPassword()!=null){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setId(UUID.randomUUID());

            List<Authoritiy> authoritiyList = new ArrayList<>();
            authoritiyList.add(new Authoritiy(UUID.randomUUID(),Role.ROLE_USER.getName(),user));

            user.setAuthorities(authoritiyList);
            userRepository.save(user);

            String url = generateToken(user,request);
            try {
                mailService.sendMail(user.getUsername(), "Verification Token", url, false);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

            if(user.getRole().equals(Role.ROLE_ADMIN)){
                String urlAdmin = generateAdminToken(user,request);
                try {
                    mailService.sendMail("vecege9838@lidte.com", "Verification Admin Token", urlAdmin, false);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private String generateToken(User user, HttpServletRequest request){
        UUID token = UUID.randomUUID();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setTokenValue(token);
        verificationToken.setUser(user);
        verificationTokenRepository.save(verificationToken);

        String url = "http://" + request.getServerName() +
                ":" +
                request.getServerPort() +
                request.getContextPath() +
                "/verify-token?token="+token;
        return url;
    }

    private String generateAdminToken(User user, HttpServletRequest request){
        UUID token = UUID.randomUUID();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setTokenValue(token);
        verificationToken.setUser(user);
        verificationTokenRepository.save(verificationToken);

        String url = "http://" + request.getServerName() +
                ":" +
                request.getServerPort() +
                request.getContextPath() +
                "/verify-admin?adminToken="+token;
        return url;
    }

    public void verifyToken(String token) {
        User appUser = verificationTokenRepository.findByTokenValue(UUID.fromString(token)).getUser();
        appUser.setEnabled(true);
        userRepository.save(appUser);
        verificationTokenRepository.deleteById(UUID.fromString(token));

    }

    public void verifyAdmin(String adminToken) {
        User appUser = verificationTokenRepository.findByTokenValue(UUID.fromString(adminToken)).getUser();
        authoritiyRepository.save(new Authoritiy(UUID.randomUUID(),Role.ROLE_ADMIN.getName(),appUser));
    }
}
